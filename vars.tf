variable "REGION" {
	default = "northamerica-northeast1"
}
variable "ZONE" {
	default = "northamerica-northeast1-c"
}
variable "PROJECT" {
	default = "equifax-demo-dev"
}
variable "ZONES" {
	default = ["northamerica-northeast1-a", "northamerica-northeast1-b", "northamerica-northeast1-c"]
}
variable "IMAGE_NAME" {
	default = "coreos-stable-2303-4-0-v20200210"
}
variable "swarm_managers_instance_type" {
	default = "n1-standard-1"
}
variable "swarm_managers" {
	default = "3"
}
variable "swarm_workers" {
	default = "2"
}
variable "swarm_workers_instance_type" {
	default = "n1-standard-1"
}
