terraform {
  backend "gcs" {
    bucket  = "equifax-terraform-states"
    prefix  = "terraform/state/docker-swarm-gcp"
  }
}
