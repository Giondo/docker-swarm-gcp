resource "google_compute_instance" "managers" {
  count        = var.swarm_managers
  name         = "manager${count.index + 1}"
  machine_type = var.swarm_managers_instance_type
  zone         = var.ZONE

  boot_disk {
    initialize_params {
      image = var.IMAGE_NAME
      size  = 100
    }
  }

  network_interface {
    network       = google_compute_network.swarm.name
    access_config {
          // Ephemeral IP
        }
  }
}
