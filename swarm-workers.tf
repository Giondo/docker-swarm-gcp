resource "google_compute_instance" "workers" {
  count        = var.swarm_workers
  name         = "worker${count.index + 1}"
  machine_type = var.swarm_workers_instance_type
  zone         = var.ZONE

  depends_on = [google_compute_instance.managers]

  boot_disk {
    initialize_params {
      image = var.IMAGE_NAME
      size  = 100
    }
  }

  # metadata {
  #   sshKeys = "${var.ssh_user}:${file(var.ssh_pub_key_file)}"
  # }

  network_interface {
    network       = google_compute_network.swarm.name
    access_config {
          // Ephemeral IP
        }
  }
}
